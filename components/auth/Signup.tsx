import React from 'react';
import styles from '../../styles/form.module.css';

function Signup() {
    return (
        <div className={styles.formHolder}>
            <form>
                <div className={styles.inputHolder}>
                    <input type="text" placeholder='Full Name' />
                </div>

                <div className={`mt-4 ${styles.inputHolder}`}>
                    <input type="email" placeholder='Email' />
                </div>

                <div className={`mt-4 ${styles.inputHolder}`}>
                    <input type="text" placeholder='Phone Number' />
                </div>

                <div className={`mt-4 ${styles.inputHolder}`}>
                    <input type="password" placeholder='Password' />
                </div>


                <div className='mt-4'>
                    <button type="submit" className='defaultBtn w-100'>Signup</button>
                </div>
            </form>
        </div>
    )
}

export default Signup
