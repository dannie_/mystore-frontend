import React from 'react';
import styles from '../../styles/form.module.css';

function Login() {
    return (
        <div className={styles.formHolder}>
            <form>
                <div className={styles.inputHolder}>
                    <input type="text" placeholder='Username' />
                </div>

                <div className={`mt-4 ${styles.inputHolder}`}>
                    <input type="text" placeholder='Password' />
                </div>

                <div className='mt-4'>
                    <button type="submit" className='defaultBtn w-100'>Login</button>
                </div>
            </form>
        </div>
    )
}

export default Login
