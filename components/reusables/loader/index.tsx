import React from 'react';
import styles from '../../../styles/landing.module.css';

interface Props {
    big: Boolean
}

const Loader = ({big}: Props) => {
    return (
        <div>

            <div className={`alignOnly mt-2 ${big && 'sectionHeight'}`}>
                <div className={`${big && styles.big} ${styles.loader}`}></div>
            </div>
        </div>
    )
}

export default Loader;
