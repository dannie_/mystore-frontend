import React from 'react';
import styles from '../../../styles/form.module.css';
import {Modal} from 'react-bootstrap';

interface Props {
    show: boolean,
    handleClose: () => void
}


function ProductModal({show, handleClose}: Props) {
 
    return (
        <div>
            <Modal show={show} onHide={handleClose} centered>
                <div className={`container ${styles.formHolder}`}>
                    <h2 className="sectionText--mid vAlign">Add Product</h2>
                    <form className='mt-4'>
                        <div className={styles.inputHolder}>
                            <input type="text" placeholder='Item Name' />
                        </div>

                        <div className={`mt-4 ${styles.inputHolder}`}>
                            <input type="text" placeholder='Item Description' />
                        </div>

                        <div className={`mt-4 ${styles.inputHolder}`}>
                            <input type="file" placeholder='Item Description' />
                        </div>

                        <div className='mt-4 center'>
                            <button type="submit" className='defaultBtn'>Upload </button>
                        </div>
                    </form>
                </div>
                </Modal>
        </div>
    )
}

export default ProductModal
