import React, {useState} from 'react';
import Link from 'next/link';
import styles from '../../../styles/header.module.css';
import {checkAuth} from '../../../utils/helpers/checkAuth';
import { FaUser, FaChevronDown, FaPlus, FaCogs, FaSignOutAlt} from 'react-icons/fa';
import ProductModal from '../productModal';

interface Props {
    openModal? : () => void,
    dark?: Boolean
}

function Header({ openModal, dark} : Props) {

    const [showDrop,
        setShow] = useState(false);

        const [showProductModal, setProduct] = useState(false);

    return (
        <div className={`alignOnly ${styles.header} ${dark && styles.dark}`}>
            <div className="container spaceBetween alignOnly">
                <h2 className="sectionHead--mid mb-0">
                    <a href="/" className="textIsBlack">My<span className={`textIsPurple ${dark && styles.dark}`}>store.</span>
                    </a>
                </h2>
                <div className={styles.actionHolder}>
                    {checkAuth()
                        ? <div
                                className='textIsPurple center clickable'
                                onClick={() => setShow(!showDrop)}>
                                <FaUser/>
                                <span className="sectionText mx-2">Ogbe Daniel</span>
                                <FaChevronDown
                                    className={` ${showDrop && styles.active} ${styles.icon} sectionText--small`}/>
                            </div>
                        : <button className="defaultBtn w-100" onClick={openModal}>Get Started</button>
}

                    <div className={`${styles.dropdown} ${showDrop && styles.active}`}>
                        <p className={styles.caret}></p>

                        <ul className={styles.content}>
                            <Link href="/profile/12">
                            <li className='sectionText'>
                                <FaUser/>
                                <span className='mx-2 sectionText'>My Profile</span>
                            </li>
                            </Link>

                            <li className='sectionText' onClick={() => setProduct(true)}>
                                <FaPlus/>
                                <span className='mx-2 sectionText'>Add Product</span>
                            </li>

                            <li className='sectionText'>
                                <FaCogs/>
                                <span className='mx-2 sectionText'>Settings</span>
                            </li>

                            <li className='sectionText'>
                                <FaSignOutAlt/>
                                <span className='mx-2 sectionText'>Logout</span>
                            </li>
                        </ul>
                    </div>
                </div >
            </div>
            <ProductModal show={showProductModal} handleClose={() => setProduct(false)}/>
        </div>
    )
}

export default Header;