import React, {useState} from 'react';
import {Modal} from 'react-bootstrap';
import Login from '../../auth/Login';
import Signup from '../../auth/Signup';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import styles from '../../../styles/landing.module.css';

interface Props {
    show: boolean,
    handleClose: () => void
}

function AuthModal({ show, handleClose}: Props) {

    return (
        <div>
            <Modal show={show} onHide={handleClose} centered>
                <div className={styles.authHolder}>
                    <Tabs>
                        <TabList>
                            <Tab>Login</Tab>
                            <Tab>Signup</Tab>
                        </TabList>

                        <TabPanel>
                            <Login/>
                        </TabPanel>
                        <TabPanel>
                       <Signup/>
                        </TabPanel>
                    </Tabs>
                </div>
              
            </Modal>
        </div>
    )
}

export default AuthModal;
