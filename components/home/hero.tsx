import React from 'react';
import Link from 'next/link';
import styles from '../../styles/landing.module.css';
import {checkAuth} from '../../utils/helpers/checkAuth';

interface Props {
    openModal : () => void
}

const Hero = ({openModal} : Props) => {
    return (
        <div className={styles.hero}>
            <div className="container">
                <div className={`alignOnly sectionHeight ${styles.hero__content}`}>
                    <div>
                        <h2 className="textIsWhite sectionText--large">Connect with both Vendors and Consumers on Mystore.</h2>
                        <p className='mt-3 mb-4 textIsWhite sectionText--mid'>A lightweight marketplace
                            for both sides of the market to connect. Enabling vendors showcase their
                            products and providing a marketplace for consumers.</p>

                        {checkAuth()
                            ? <Link href='/profile/12'>
                                    <button className='defaultBtn--white textIsWhite'>Your Profile</button>
                                </Link>
                            : <button className='defaultBtn--white textIsWhite' onClick={openModal}>Get Started</button>
}
                    </div>
                </div>

            </div>

        </div>
    )
}

export default Hero;
