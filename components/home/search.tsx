import React, {useState} from 'react';
import Image from 'next/image';
import searchImg from '../../images/search.png';
import styles from '../../styles/landing.module.css';
import {FaSearch} from 'react-icons/fa'
import Loader from '../reusables/loader';

function Search() {

    const [isLoading,
        setisLoading] = useState(false);

    const handleLoading = () => {
        setisLoading(true)
    }

    return (
        <div className='container'>
            <div className={styles.search__inputHolder}>
                <form>
                    <input type="text" placeholder='Search everything'/>

                    <FaSearch onClick={handleLoading} className='textIsPurple'/>
                </form>
            </div>

            <div className={`sectionHeight--mid`}>
                {isLoading
                    ? <Loader big/>
                    : <Image src={searchImg} alt='search section Image'/>
}

            </div>
        </div>
    )
}

export default Search;
