import React, {useState} from 'react';
import Image from 'next/image';
import profile from '../../images/profile.jpeg'
import Header from '../../components/reusables/header'
import styles from '../../styles/profile.module.css'
import ProductModal from '../../components/reusables/productModal';

function Profile() {

    const [show, setShow] = useState(false);

    return (
        <div>
            <Header/>

            <div className={styles.hero}>
                <div className="container alignOnly">
    
                </div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-lg-3">
                        <div className={styles.imgHolder}>
                            <Image src={profile} alt="Profile picture"/>
                        </div>
                    </div>
                    <div className="col-lg-9 sectionHeight--mid">
                        <div className="w-100 spaceBetween mBlock">

                            <div className="mt-3">
                                <h2 className="sectionHead textIsBlack mb-2">Ogbe Daniel</h2>
                                <p className="sectionText textIsPurple mb-2">Username</p>
                                <p className="sectionText textIsPurple mb-2">Email</p>
                                <p className="sectionText textIsPurple">Phone</p>
                            </div>

                            <button className='defaultBtn'>Update Personal Details</button>
                        </div>


                        <div className="mt-5">
                            <div className="spaceBetween">
                                <h2 className="sectionHead--mid">Your Products</h2>
                                <button className='defaultBtn' onClick={() => setShow(true)}>Add Product</button>
                            </div>

                            <div className="sectionHeight--short center">
                                <p className='sectionText textIsPurple'>No products</p>
                            </div>
                        </div>
                    </div>
                </div>
                <ProductModal show={show} handleClose={() => setShow(false)}/>
            </div>

        </div>
    )
}

export default Profile
