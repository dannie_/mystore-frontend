import React from 'react';
import Homepage from './index'
import '../styles/global.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App({Component}) {
    return (
        <div>
          <Component/>
        </div>
    )
}

export default App;
