import React, {useState} from "react";
import Hero from "../components/home/hero";
import Search from "../components/home/search";
import AuthModal from "../components/reusables/authModal";
import Header from "../components/reusables/header";

function HomePage() {
    const [show, setShow] = useState(false);
return(
    <div>
        <Header openModal={() => setShow(true)}/>
        <Hero openModal={() => setShow(true)}/>
        <Search/>
        <AuthModal show={show} handleClose={() => setShow(false)}/>
    </div>
)
}

export default HomePage