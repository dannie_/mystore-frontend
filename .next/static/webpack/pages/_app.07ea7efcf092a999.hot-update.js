/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
self["webpackHotUpdate_N_E"]("pages/_app",{

/***/ "./components/home/hero.tsx":
/*!**********************************!*\
  !*** ./components/home/hero.tsx ***!
  \**********************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

/* module decorator */ module = __webpack_require__.nmd(module);


;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ }),

/***/ "./pages/index.tsx":
/*!*************************!*\
  !*** ./pages/index.tsx ***!
  \*************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"./node_modules/react/jsx-dev-runtime.js\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"./node_modules/react/index.js\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _components_home_hero__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/home/hero */ \"./components/home/hero.tsx\");\n/* harmony import */ var _components_home_hero__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_home_hero__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _components_home_search__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/home/search */ \"./components/home/search.tsx\");\n/* harmony import */ var _components_reusables_authModal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/reusables/authModal */ \"./components/reusables/authModal/index.tsx\");\n/* harmony import */ var _components_reusables_header__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/reusables/header */ \"./components/reusables/header/index.tsx\");\n/* module decorator */ module = __webpack_require__.hmd(module);\n\n\n\n\n\n\nvar _s = $RefreshSig$();\nfunction HomePage() {\n    _s();\n    var ref = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false), show = ref[0], setShow = ref[1];\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_reusables_header__WEBPACK_IMPORTED_MODULE_5__[\"default\"], {\n                openModal: function() {\n                    return setShow(true);\n                }\n            }, void 0, false, {\n                fileName: \"/home/okeogbe/workspace/projects/mystore-frontend/pages/index.tsx\",\n                lineNumber: 11,\n                columnNumber: 9\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((_components_home_hero__WEBPACK_IMPORTED_MODULE_2___default()), {\n                openModal: function() {\n                    return setShow(true);\n                }\n            }, void 0, false, {\n                fileName: \"/home/okeogbe/workspace/projects/mystore-frontend/pages/index.tsx\",\n                lineNumber: 12,\n                columnNumber: 9\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_home_search__WEBPACK_IMPORTED_MODULE_3__[\"default\"], {}, void 0, false, {\n                fileName: \"/home/okeogbe/workspace/projects/mystore-frontend/pages/index.tsx\",\n                lineNumber: 13,\n                columnNumber: 9\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_reusables_authModal__WEBPACK_IMPORTED_MODULE_4__[\"default\"], {\n                show: show,\n                handleClose: function() {\n                    return setShow(false);\n                }\n            }, void 0, false, {\n                fileName: \"/home/okeogbe/workspace/projects/mystore-frontend/pages/index.tsx\",\n                lineNumber: 14,\n                columnNumber: 9\n            }, this)\n        ]\n    }, void 0, true, {\n        fileName: \"/home/okeogbe/workspace/projects/mystore-frontend/pages/index.tsx\",\n        lineNumber: 10,\n        columnNumber: 5\n    }, this));\n}\n_s(HomePage, \"Hdw5EO+DplCNBEJcNuH8tsP7WZ4=\");\n_c = HomePage;\n/* harmony default export */ __webpack_exports__[\"default\"] = (HomePage);\nvar _c;\n$RefreshReg$(_c, \"HomePage\");\n\n\n;\n    var _a, _b;\n    // Legacy CSS implementations will `eval` browser code in a Node.js context\n    // to extract CSS. For backwards compatibility, we need to check we're in a\n    // browser context before continuing.\n    if (typeof self !== 'undefined' &&\n        // AMP / No-JS mode does not inject these helpers:\n        '$RefreshHelpers$' in self) {\n        var currentExports = module.__proto__.exports;\n        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;\n        // This cannot happen in MainTemplate because the exports mismatch between\n        // templating and execution.\n        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);\n        // A module can be accepted automatically based on its exports, e.g. when\n        // it is a Refresh Boundary.\n        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {\n            // Save the previous exports on update so we can compare the boundary\n            // signatures.\n            module.hot.dispose(function (data) {\n                data.prevExports = currentExports;\n            });\n            // Unconditionally accept an update to this module, we'll check if it's\n            // still a Refresh Boundary later.\n            module.hot.accept();\n            // This field is set when the previous version of this module was a\n            // Refresh Boundary, letting us know we need to check for invalidation or\n            // enqueue an update.\n            if (prevExports !== null) {\n                // A boundary can become ineligible if its exports are incompatible\n                // with the previous exports.\n                //\n                // For example, if you add/remove/change exports, we'll want to\n                // re-execute the importing modules, and force those components to\n                // re-render. Similarly, if you convert a class component to a\n                // function, we want to invalidate the boundary.\n                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {\n                    module.hot.invalidate();\n                }\n                else {\n                    self.$RefreshHelpers$.scheduleUpdate();\n                }\n            }\n        }\n        else {\n            // Since we just executed the code for the module, it's possible that the\n            // new exports made it ineligible for being a boundary.\n            // We only care about the case when we were _previously_ a boundary,\n            // because we already accepted this update (accidental side effect).\n            var isNoLongerABoundary = prevExports !== null;\n            if (isNoLongerABoundary) {\n                module.hot.invalidate();\n            }\n        }\n    }\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC50c3guanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQXFDO0FBQ0s7QUFDSTtBQUNXO0FBQ047O1NBRTFDTSxRQUFRLEdBQUcsQ0FBQzs7SUFDakIsR0FBSyxDQUFtQkwsR0FBZSxHQUFmQSwrQ0FBUSxDQUFDLEtBQUssR0FBL0JNLElBQUksR0FBYU4sR0FBZSxLQUExQk8sT0FBTyxHQUFJUCxHQUFlO0lBQzNDLE1BQU0sNkVBQ0RRLENBQUc7O3dGQUNDSixvRUFBTTtnQkFBQ0ssU0FBUyxFQUFFLFFBQVE7b0JBQUZGLE1BQU0sQ0FBTkEsT0FBTyxDQUFDLElBQUk7Ozs7Ozs7d0ZBQ3BDTiw4REFBSTtnQkFBQ1EsU0FBUyxFQUFFLFFBQVE7b0JBQUZGLE1BQU0sQ0FBTkEsT0FBTyxDQUFDLElBQUk7Ozs7Ozs7d0ZBQ2xDTCwrREFBTTs7Ozs7d0ZBQ05DLHVFQUFTO2dCQUFDRyxJQUFJLEVBQUVBLElBQUk7Z0JBQUVJLFdBQVcsRUFBRSxRQUFRO29CQUFGSCxNQUFNLENBQU5BLE9BQU8sQ0FBQyxLQUFLOzs7Ozs7Ozs7Ozs7O0FBRy9ELENBQUM7R0FWUUYsUUFBUTtLQUFSQSxRQUFRO0FBWWpCLCtEQUFlQSxRQUFRIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vX05fRS8uL3BhZ2VzL2luZGV4LnRzeD8wN2ZmIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwge3VzZVN0YXRlfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBIZXJvIGZyb20gXCIuLi9jb21wb25lbnRzL2hvbWUvaGVyb1wiO1xuaW1wb3J0IFNlYXJjaCBmcm9tIFwiLi4vY29tcG9uZW50cy9ob21lL3NlYXJjaFwiO1xuaW1wb3J0IEF1dGhNb2RhbCBmcm9tIFwiLi4vY29tcG9uZW50cy9yZXVzYWJsZXMvYXV0aE1vZGFsXCI7XG5pbXBvcnQgSGVhZGVyIGZyb20gXCIuLi9jb21wb25lbnRzL3JldXNhYmxlcy9oZWFkZXJcIjtcblxuZnVuY3Rpb24gSG9tZVBhZ2UoKSB7XG4gICAgY29uc3QgW3Nob3csIHNldFNob3ddID0gdXNlU3RhdGUoZmFsc2UpO1xucmV0dXJuKFxuICAgIDxkaXY+XG4gICAgICAgIDxIZWFkZXIgb3Blbk1vZGFsPXsoKSA9PiBzZXRTaG93KHRydWUpfS8+XG4gICAgICAgIDxIZXJvIG9wZW5Nb2RhbD17KCkgPT4gc2V0U2hvdyh0cnVlKX0vPlxuICAgICAgICA8U2VhcmNoLz5cbiAgICAgICAgPEF1dGhNb2RhbCBzaG93PXtzaG93fSBoYW5kbGVDbG9zZT17KCkgPT4gc2V0U2hvdyhmYWxzZSl9Lz5cbiAgICA8L2Rpdj5cbilcbn1cblxuZXhwb3J0IGRlZmF1bHQgSG9tZVBhZ2UiXSwibmFtZXMiOlsiUmVhY3QiLCJ1c2VTdGF0ZSIsIkhlcm8iLCJTZWFyY2giLCJBdXRoTW9kYWwiLCJIZWFkZXIiLCJIb21lUGFnZSIsInNob3ciLCJzZXRTaG93IiwiZGl2Iiwib3Blbk1vZGFsIiwiaGFuZGxlQ2xvc2UiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/index.tsx\n");

/***/ })

});